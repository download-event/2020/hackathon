# Hackathon

## Tema - ITA:

Le api sono uno degli insetti alla base dell'ecosistema terreste grazie alla loro capacità di impollinare. Svolgono perciò un ruolo molto importante nella salvaguardia della biodiversità del pianeta e ci permettono di assaporare il 70% di tutte le prelibatezze vegetali, e non, che troviamo sulle nostre tavole. 
Si organizzano autonomamente in maniera gerarchica. Tutte hanno una loro funzione all'interno della colonia. Ad esempio le api operaie assumono, in base all'anzianità, diverse mansioni come la pulizia della colonia, la ricerca del nettare o del polline, il nutrimento delle larve oppure la difesa della colonia da intrusi esterni. 
Un'altra peculiarità delle api è il loro modo di comunicare unico attraverso diverse danze. 
Ad esempio per comunicare la posizione dei fiori si cimentano in una danza e le altre api, con estrema precisione, deducono la posizione dei fiori dalla durata e dall’orientazione della danza.

Durante questi tempi senza precedenti stiamo affrontando molte sfide e stiamo imparando a conoscere i limiti degli strumenti che utilizziamo per comunicare. 
Quindi .. 
Ispirandovi al naturale e meraviglioso mondo delle api, ad esempio alla loro abilità di essere quasi totalmente decentralizzate, oppure di comunicare su molti canali o di organizzarsi autonomamente, vi viene chiesto di creare un progetto per raggiungere e comunicare con le persone.

## Theme - ENG:

Bees are one of the insects at the foundation of the earth's ecosystem thanks to their ability to pollinate. They play a very important role in safeguarding the planet's biodiversity and therefore allow us to taste 70% of all the vegetable and non-vegetable delicacies that we find on our tables.
They organize themselves in an hierarchical and autonomous way. It’s not only the queen that makes decisions but sometimes all the colony makes the decision together in a way that is still not completely known by man. That’s why we hear the term superorganism when talking about bees.
They all have their own function within the colony. For example, worker bees take on, based on their seniority, various tasks such as cleaning the hive, searching for nectar or pollen, feeding the larvae or defending the colony from external intruders.
Another peculiarity of bees is their unique way of communicating with each other by engaging in different dances. For example, to communicate the location of flowers they perform a dance and the other bees, with extreme precision, deduce the position from the orientation and duration of the dance.

During these unprecedented times we are facing many challenges and we have come to know all the limitations of the tools we are using to communicate.
So… 
Inspired by the natural and wonderful world of bees, for example their ability to be almost totally decentralized, or to communicate on many channels or to organize themselves autonomously, you are asked to create a project to reach and communicate with people.

